import spacy
from spacy import displacy
from nltk.stem.porter import PorterStemmer
from nltk.stem.snowball import SnowballStemmer
from spacy.matcher import Matcher
from spacy.matcher import PhraseMatcher

nlp = spacy.load('en_core_web_sm')
with open('D:\\ASHISH\\UPDATED_NLP_COURSE\\TextFiles\\owlcreek.txt') as f:
    doc = nlp(f.read())
print(doc[:36])
print(len(doc))

sents = [sent for sent in doc.sents]
print(len(sents))
print(sents[2].text)

for token in sents[2]:
    print(token.text, token.pos_, token.dep_, token.lemma_)
for token in sents[2]:
    print(f'{token.text:{15}} {token.pos_:{5}} {token.dep_:{10}} {token.lemma_:{15}}')

matcher = Matcher(nlp.vocab)
pattern = [{'LOWER': 'swimming'}, {'IS_SPACE': True, 'OP': '*'}, {'LOWER': 'vigorously'}]
matcher.add('Swimming', None, pattern)
found_matches = matcher(doc)
print(found_matches)
print(doc[1270:1280])
print(doc[3600:3615])
for sent in sents:
    if found_matches[0][1] < sent.end:
        print(sent)
        break
for sent in sents:
    if found_matches[1][1] < sent.end:
        print(sent)
        break
