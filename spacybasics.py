import spacy
from spacy import displacy
from nltk.stem.porter import PorterStemmer
from nltk.stem.snowball import SnowballStemmer
from spacy.matcher import Matcher
from spacy.matcher import PhraseMatcher

nlp = spacy.load('en_core_web_sm')
doc1 = nlp(u"BNP PARIBAS ISPL is looking at buying India's startup for $6 million.")
for token in doc1:
    print(token.text, token.pos_, token.dep_)

print(nlp.pipeline)
print(nlp.pipe_names)

doc2 = nlp(u"BNP PARIBAS ISPL    isn't looking at buying India's startups anymore.")
for token in doc2:
    print(token.text, token.pos_, token.dep_)
print(doc2, doc2[0:3], type(doc2[0:3]))
print(type(doc2))
print(spacy.explain('PROPN'))
print(doc2[6].text)
print(doc2[6].lemma_)
print(doc2[6].shape_)
print(doc2[6].tag_ + ' / ' + spacy.explain(doc2[4].tag_))
print(doc2[6].is_alpha)
print(doc2[6].is_stop)

doc3 = nlp(u'This is my first poc of nlp. This will help me get the project. Budget is limited but we do not care.')
for sent in doc3.sents:
    print(sent)
print(doc3[8].is_sent_start)

doc4 = nlp(u'"We\'re moving to U.S.A.!"')
for token in doc4:
    print(token.text, end=' | ')
print(end='\n')
print(doc4[-3:], len(doc4), len(doc4.vocab))
doc5 = nlp(u'BNP PARIBAS ISPL is ready to setup an office in Nirlon Technological Park')

for token in doc5:
    print(token.text, end=' | ')

print('\n------------')

for ent in doc5.ents:
    print(ent.text+' - '+ent.label_+' - '+str(spacy.explain(ent.label_)))

doc6 = nlp(u"He was a one-eyed, one-horned, flying, purple people-eater.")
for chunk in doc6.noun_chunks:
    print(chunk.text)

doc = nlp(u"BNP PARIBAS ISPL is looking at buying India's startup for $6 million.")
#displacy.serve(doc, style='dep', options={'distance': 110})

p_stemmer = PorterStemmer()
words = ['run', 'runner', 'running', 'ran', 'runs', 'easily', 'fairly']
for word in words:
    print(word+' --> '+p_stemmer.stem(word))
s_stemmer = SnowballStemmer(language='english')
for word in words:
    print(word+' --> '+s_stemmer.stem(word))


def show_lemmas(text):
    for tok in text:
        print(f'{tok.text:{12}} {tok.pos_:{6}} {tok.lemma:<{22}} {tok.lemma_}')


doc7 = nlp(u"I am meeting her tomorrow at the meeting.")
show_lemmas(doc7)
doc8 = nlp(u"That's an enormous electric car by Tesla")
show_lemmas(doc8)
print(nlp.Defaults.stop_words)
print(nlp.vocab['myself'].is_stop)
nlp.Defaults.stop_words.add('btw')
nlp.vocab['btw'].is_stop = True
nlp.Defaults.stop_words.remove('beyond')
nlp.vocab['beyond'].is_stop = False

matcher = Matcher(nlp.vocab)
pattern1 = [{'LOWER': 'solarpower'}]
pattern2 = [{'LOWER': 'solar'}, {'LOWER': 'power'}]
pattern3 = [{'LOWER': 'solar'}, {'IS_PUNCT': True}, {'LOWER': 'power'}]

matcher.add('SolarPower', None, pattern1, pattern2, pattern3)
doc9 = nlp(u'The Solar Power industry continues to grow as demand for solarpower increases. Solar-power cars are gaining popularity.')
found_matches = matcher(doc9)
for match_id, start, end in found_matches:
    string_id = nlp.vocab.strings[match_id]
    span = doc9[start:end]
    print(match_id, string_id, start, end, span.text)
matcher.remove('SolarPower')
pattern1 = [{'LOWER': 'solarpower'}]
pattern2 = [{'LOWER': 'solar'}, {'IS_PUNCT': True, 'OP': '*'}, {'LOWER': 'power'}]
matcher.add('SolarPower', pattern1, pattern2)
matcher.add('SolarPower', None, pattern1, pattern2)
found_matches = matcher(doc9)
matcher.remove('SolarPower')
pattern1 = [{'LOWER': 'solarpower'}]
pattern2 = [{'LOWER': 'solar'}, {'IS_PUNCT': True, 'OP': '*'}, {'LEMMA': 'power'}]
matcher.add('SolarPower', None, pattern1, pattern2)
doc10 = nlp(u'Solar-powered energy runs solar-powered cars.')
found_matches = matcher(doc10)
print(found_matches)
for match_id, start, end in found_matches:
    string_id = nlp.vocab.strings[match_id]
    span = doc10[start:end]
    print(match_id, string_id, start, end, span.text)
phrase_matcher = PhraseMatcher(nlp.vocab)
with open('D:\\ASHISH\\UPDATED_NLP_COURSE\\TextFiles\\reaganomics.txt') as f:
    doc11 = nlp(f.read())
phrase_list = ['voodoo economics', 'supply-side economics', 'trickle-down economics', 'free-market economics']
phrase_patterns = [nlp(text) for text in phrase_list]
phrase_matcher.add('VoodooEconomics', None, *phrase_patterns)
matches = phrase_matcher(doc11)
print(matches)
displacy.serve(doc, style='ent', options={'distance': 110})
